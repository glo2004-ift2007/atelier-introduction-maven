## Atelier d'introduction à l'outil de gestion des dépendances _Maven_

Durant cet atelier, vous serez introduits à un outil de gestion de dépendances, _Maven_. Cet outil permet de gérer la compilation d'un projet _Java_ ainsi que de gérer les dépendances (librairies externes) que vous pourriez vouloir utiliser dans votre projet. 

À partir d’une base de code fournie, vous apprendrez à utiliser  _Maven_ pour compiler la base de code ainsi que pour ajouter des dépendances. Vous apprendrez également à générer un _jar_ fonctionnel de la base de code. _Maven_ utilise un fichier de configuration qui se nomme le _pom.xml_ (présent par défaut lorsque vous générez un nouveau projet avec _Maven_ dans un _IDE_). 

Ce fichier est un _xml_ représentant la configuration _Maven_ à utiliser pour le projet. Notez que pour l’instant le _pom.xml_ est presque vide et ne comporte que des balises de base qui permettent d’identifier le nom de votre projet, la version de _Maven_ à utiliser ainsi que d’autres métadonnées du projet. Ouvrez-le et prenez le temps de vous familiariser avec celui-ci, c’est ce fichier que vous modifierez tout au long de l’atelier. Vous pouvez ensuite suivre les étapes ci-dessous pour apprendre comment compiler le projet avec _Maven_, ajouter des dépendances et générer un _jar_ fonctionnel.

1. Assurez-vous aussi d'avoir l'IDE _NetBeans_ et _Java 17_ d'installés, sinon veuillez suivre l'atelier _Installation de NetBeans et prise en main de l'IDE_ disponible sur le wiki du cours en suivant ce [lien](http://www2.ift.ulaval.ca/gaudreault/dokuwiki_a14/doku.php?id=labs:fruitbasket:a0) (nom d'utilisateur : etudiants, mot de passe : etudiantsulaval). Veuillez noter que cet atelier peut être réalisé avec n'importe quel IDE mais les commandes sont détaillées spécifiquement en utilisant _NetBeans_ afin de simplifier les explications. Téléchargez la base de code fournie dans ce répertoire. 

2. Ouvrez le projet dans l'IDE, cliquez droit sur le projet dans la colonne de gauche, sélectionnez ensuite _Run Maven -> Goals_ et dans ce champ entrez la commande _Maven_ ci-dessous avant de l'exécuter.

    ```
    clean install
    ```
    

3. 
    a) Allez dans le répertoire _target_, vous y trouverez un fichier _atelier_introduction_maven-1.0-SNAPSHOT.jar_ 

    b) Double cliquez dessus pour essayer de l'ouvrir, cela ne sera pas possible.

    c) Essayez maintenant de l'ouvrir en ligne de commande à partir du répertoire qui contient le _jar_

    ```
    java.exe -jar atelier_introduction_maven-1.0-SNAPSHOT.jar
    ```
    De cette manière, vous pouvez constater l'erreur qui vous empêche d'exécuter correctement le _jar_

    ```
    no main manifest attribute
    ```
    Ceci est normal puisque les métadonnées de la classe _main_ n'ont pas encore été spécifiées.
    
4. Ouvrez maintenant le fichier _pom.xml_ et copiez le _xml_ suivant à l'intérieur de la balise _project_.
    ```
    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.1</version>
            </plugin>
        </plugins>
    </build>
    ```
    Le _Compiler Plugin_ version 3.8.1 sert à compiler le code source du projet, lorsqu'il est inclus dans le projet _Maven_ de cette façon, il est exécuté automatiquement lorsque le projet le sera lui aussi. Les plugins _Maven_ sont des outils permettant d’effectuer certaines étapes supplémentaires lors de la compilation (_build_) du projet. Dans cet exemple, le plugin _maven-compiler-plugin_ a été ajouté qui permet de compiler votre projet (convertir vos classes _.java_ en classes compilées _.class_).

5. Remarquez qu'il est impératif d'avoir la bonne structure de fichiers tels que fournis dans la base de code.
    ```
    src/main/java/ca/ulaval/glo2004
    ```

6. Ajoutez ensuite le plugin _org.codehaus.mojo_ dans le fichier _pom.xml_ à la suite du _plugin_ et à l'intérieur de la balise _plugins_ qui ont été ajoutés à l'étape précédente. Celui-ci permet de spécifier à quel endroit se trouve la classe à exécuter (_ca.ulaval.glo2004.Main_) et de la compiler. _Mojo_ signifie _**M**aven plain **O**ld **J**ava **O**bject_, c'est donc quelque chose qui permet d'étendre une fonctionnalité de la phase de _build_ qui n'est pas disponible de base dans _Maven_.

    ```
    <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>exec-maven-plugin</artifactId>
        <version>3.1.0</version>
        <executions>
            <execution>
                <goals>
                    <goal>java</goal>
                </goals>
            </execution>
        </executions>
        <configuration>
            <mainClass>
                ca.ulaval.glo2004.Main
            </mainClass>
        </configuration>
    </plugin>
    ```
    
7. Ajoutez ensuite le plugin _maven-jar-plugin_, toujours à l'intérieur de la balise _plugins_, qui permet finalement de compiler le _jar_ en configurant le fichier _manifest_ qui contient les métadonnées telles que la _main class_, les dépendances et autres.
    
    ```
    <plugin>
        <artifactId>maven-jar-plugin</artifactId>
        <version>3.0.2</version>
        <configuration>
            <archive>
                <manifest>
                    <mainClass>ca.ulaval.glo2004.Main</mainClass>
                </manifest>
            </archive>
        </configuration>
    </plugin>
    ```

8. Vous pouvez maintenant ajouter des dépendances à votre projet, allez sur un site tel que _central.sonatype.com_ et recherchez _flatlaf_. Copiez ensuite la section _snippets_ en prenant soin d'ajoutez la balise des dépendances nommée _dependencies_ à l'intérieur de la balise _project_. Les autres dépendances que vous aurez à ajouter devront se retrouver à l'intérieure de la balise des dépendances.

    ```
    <dependencies>
        <dependency>
            <groupId>com.formdev</groupId>
            <artifactId>flatlaf</artifactId>
            <version>3.1.1</version>
        </dependency>
    </dependencies>
    ```

9. Exécutez ensuite à nouveau le fichier _jar_  en utilisant la méthode décrite précédemment, cela vous permettra de constater que les modifications apportées au fichier _pom.xml_ fonctionnent.

10. Une fois l'atelier complété, vous pouvez comparer ce que vous avez obtenu avec le corrigé disponible dans ce même répertoire sous la branche _solution_. N'hésitez pas à vous référer à cet atelier lors de la réalisation de votre projet.

### Avantages par rapport à la compilation manuelle
L’utilisation de _Maven_ permet d’éviter de compiler manuellement votre projet. Cela permet entre autres que toutes vos dépendances soient ajoutées automatiquement à votre projet lors de la compilation et de vous éviter de devoir télécharger et gérer les différentes versions de celle-ci. _Maven_ va également s’occuper d’aller télécharger les dépendances des dépendances pour vous. Il permet également de simplifier la compilation de votre code. En effet, il est possible de compiler manuellement avec la commande _javac_, mais cela requiert de passer en paramètre toutes les classes de votre projet et vous devez manuellement ajouter des dépendances au besoin, ce qui demande beaucoup de temps si vous avez un projet avec plusieurs dizaines de classes. À titre d’exemple, voici les commandes qu’il faut exécuter pour compiler manuellement (sans _Maven_) le projet (à rouler dans le répertoire _atelier-introduction-maven/src/main/java/ca/ulaval/glo2004_):
```
javac.exe -d ./build  Main.java gui/MainWindow.java gui/DrawingPanel.java domain/basket/Basket.java domain/basket/BasketController.java domain/basket/BasketItem.java domain/drawing/BasketDrawer.java
```

Cela a eu pour effet de compiler toutes vos classes dans le répertoire _build_. La prochaine étape est de générer le _jar_ manuellement. Pour se faire il vous faut aller dans le répertoire _build_ et rouler la commande suivante (le paramètre _cvf_ est simplement pour spécifier que les classes à inclure dans le _build_ sont dans le répertoire courant et que la classe principale (à exécuter lors de l’ouverture du _jar_) est la classe _ca.ulaval.glo2004.Main_ 
```
jar cvf project.jar * ca.ulaval.glo2004.Main
```
