package ca.ulaval.glo2004.domain.basket;

import java.awt.*;

public interface BasketItemReadOnly {
    Point getPoint();
    Color getColor();
    int getRadius();
    boolean isSelected();
}