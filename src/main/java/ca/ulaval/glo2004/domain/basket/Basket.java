package ca.ulaval.glo2004.domain.basket;

import java.awt.Point;
import java.util.LinkedList;
import java.util.List;

public class Basket {
	
	private List<BasketItem> itemList;
	
	public Basket(){
		itemList = new LinkedList<BasketItem>();
	}

	public void add(BasketItem item){
		itemList.add(item);
	}

	public List<BasketItem> getFruitList() {
		return itemList;
	}

    void switchSelectionStatus(double x, double y) {
        for (BasketItem item : this.itemList) {
                if (item.contains(x, y)) {
                        item.switchSelection();
                }
        }    
    }

    void updateSelectedItemsPosition(Point delta) {
        for (BasketItem item : this.itemList) {
                if (item.isSelected()) {
                        item.translate(delta);
                }
        }    
    }
}
