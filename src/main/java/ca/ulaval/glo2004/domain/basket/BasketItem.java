package ca.ulaval.glo2004.domain.basket;

import java.awt.Color;
import java.awt.Point;

public class BasketItem implements BasketItemReadOnly {

    private Point point;
    private Color color;
    private int radius;
    private boolean selectionStatus;

    public BasketItem(Point point, Color color) {
            this.point = point;
            this.color = color;
            this.radius = 100;
            this.selectionStatus = false;
    }

    @Override
    public Point getPoint() {
            return point;
    }

    @Override
    public Color getColor() {
        return color;
    }

    boolean contains(double x, double y) {
        return (xIsInsideItemWidth(x) && yIsInsideItemHeight(y));
    }
    
    private boolean xIsInsideItemWidth(double x) {
            return (x < point.getX() + (radius)) && (x > point.getX() - (radius));
    }

    private boolean yIsInsideItemHeight(double y) {
            return (y < point.getY() + (radius)) && (y > point.getY() - (radius));
    }

    void switchSelection() {
            this.selectionStatus = !this.selectionStatus;
    }

    @Override
    public int getRadius() {
        return this.radius;
    }

    @Override
    public boolean isSelected() {
        return this.selectionStatus;
    }

    void translate(Point delta) {
        this.point.x = this.point.x + delta.x;
        this.point.y = this.point.y + delta.y;
    }
}